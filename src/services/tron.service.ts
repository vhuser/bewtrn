import { Injectable } from '@nestjs/common';
import { TronWeb } from 'tronweb';
import * as dotenv from 'dotenv';
dotenv.config();
import contractABI from '../services/contract-abi.json';

const contractAddress = process.env.CONTRACT_ADDRESS;
const PRIVATE_KEY = process.env.PRIVATE_KEY;

interface MyContract {
  balanceOf(address: string): Promise<number>;
  transfer(to: string, amount: number): Promise<any>;
  approve(delegate: string, amount: number): Promise<any>;
  transferFrom(from: string, to: string, amount: number): Promise<any>;
}

@Injectable()
export class TronService {
  private tronWeb: TronWeb;
  private contract: MyContract;

  constructor() {
    this.tronWeb = new TronWeb({
      fullHost: 'https://api.trongrid.io',
      headers: { 'TRON-PRO-API-KEY': process.env.TRON_API_KEY },
      privateKey: PRIVATE_KEY,
    });
    this.initializeContract();
  }

  private async initializeContract() {
    const contract = await this.tronWeb.contract(contractABI, contractAddress);
    this.contract = contract as unknown as MyContract;
  }

  async getContractInstance() {
    const contract = await this.tronWeb.contract(contractABI, contractAddress);
    return contract as unknown as MyContract;
  }

  async getBalance(tokenOwner: string) {
    const contract = await this.getContractInstance();
    const balance = await contract.balanceOf(tokenOwner);
    return this.tronWeb.fromSun(balance);
  }

  async transfer(receiver: string, numTokens: number) {
    const contract = await this.getContractInstance();
    const transaction = await contract.transfer(receiver, numTokens);
    const result = await transaction.send({
      shouldPollResponse: true,
      callValue: 0,
    });

    return result;
  }

  async approve(delegate: string, numTokens: number): Promise<any> {
    const contract = await this.getContractInstance();
    const approve = await contract.approve(delegate, numTokens);
    const result = await approve.send({
      shouldPollResponse: true,
      callValue: 0,
    });
    return result;
  }

  async transferFrom(
    owner: string,
    buyer: string,
    numTokens: number,
  ): Promise<any> {
    const contract = await this.getContractInstance();
    const transferFrom = await contract.transferFrom(owner, buyer, numTokens);
    const result = await transferFrom.send({
      shouldPollResponse: true,
      callValue: 0,
    });
    return result;
  }
}
