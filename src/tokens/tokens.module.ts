import { Module } from '@nestjs/common';
import { TokensController } from './tokens.controller';
import { TronService } from '../services/tron.service';

@Module({
  controllers: [TokensController],
  providers: [TronService],
})
export class TokensModule {}
