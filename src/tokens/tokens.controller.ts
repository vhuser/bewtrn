import { Controller, Get, Post, Body, Param } from '@nestjs/common';
import { TronService } from '../services/tron.service';

@Controller('tokens')
export class TokensController {
  constructor(private readonly tronService: TronService) {}

  @Get('balance/:address')
  async getBalance(@Param('address') address: string) {
    return this.tronService.getBalance(address);
  }

//   @Post('transfer')
//   async transfer(@Body() transferDto: { fromAddress: string; toAddress: string; amount: number }) {
//     return this.tronService.transfer(transferDto.fromAddress, transferDto.toAddress, transferDto.amount);
//   }

//   @Post('approve')
//   async approve(@Body() approveDto: { ownerAddress: string; spenderAddress: string; amount: number }) {
//     return this.tronService.approve(approveDto.ownerAddress, approveDto.spenderAddress, approveDto.amount);
//   }

//   @Post('transferFrom')
//   async transferFrom(@Body() transferFromDto: { senderAddress: string; recipientAddress: string; amount: number }) {
//     return this.tronService.transferFrom(transferFromDto.senderAddress, transferFromDto.recipientAddress, transferFromDto.amount);
//   }
}
